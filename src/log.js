import path from 'path';
import rfs from 'rotating-file-stream';
import morgan from 'morgan';
import cluster from 'cluster';
import {TraceUtils} from '@themost/common';
// setup logger directory
let logDir = path.join(process.cwd(), 'log');
let accessLogStream;
// default log filename
let accessLogFile;
if (cluster.isMaster) {
    accessLogFile = 'access.log';
    TraceUtils.log(`Initializing master access log stream ${accessLogFile} for process [${process.pid}]`);
    // create a rotating write stream
    accessLogStream = rfs(accessLogFile, {
        interval: '1d', // rotate daily
        path: logDir
    });
} else if (cluster.isWorker) {
    // get access log filename e.g. access-00.log or access-01.log etc
    // based on process.env.NODE_APP_INSTANCE e.g. NODE_APP_INSTANCE="0"
    accessLogFile = `access-${('00' + process.env.NODE_APP_INSTANCE).slice(-2)}.log`;
    TraceUtils.log(`Initializing worker access log stream ${accessLogFile} for process [${process.pid}]`);
    // create a rotating write stream
    accessLogStream = rfs(accessLogFile, {
        interval: '1d', // rotate daily
        path: logDir
    });
}

// handle rotated event
accessLogStream.on('rotated', file => {
    TraceUtils.log(`Access log was successfully rotated for process [${process.pid}]. ${file} produced by this rotation.`);
});

// custom morgan tokens
// setup morgan token remote-addr
morgan.token('x-forwarded-for', function (req) {
    return req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || (req.connection ? req.connection.remoteAddress : req.socket.remoteAddress);
});

// setup morgan token for user
morgan.token('remote-user', function (req) {
    if (typeof req.context === 'object') {
        if (req.context && req.context.interactiveUser) {
            return req.context.interactiveUser.name;
        }
        if (req.context && req.context.user) {
            return req.context.user.name;
        }
        return 'anonymous';
    }
    return 'unknown'
});

// export access log format
export const ACCESS_LOG_FORMAT = ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time[0] ":referrer" ":user-agent" ":x-forwarded-for"';

/**
 * Gets application access log handler in order to use it as a middleware in express
 * @returns {RequestHandler}
 */
export function accessLog() {
    return morgan(ACCESS_LOG_FORMAT, { stream: accessLogStream,
        // eslint-disable-next-line no-unused-vars
        skip(req, res) {
            return req.method === 'OPTIONS';
        }
    });
}

/**
 * Writes an entry in application access log file based on the given request and response
 * @param {LogRequest} req
 * @param {LogResponse} res
 * @param {*} startAt
 * @param {*} endAt
 */
export function writeAccessLog(req, res, startAt, endAt) {
    // get ena time (or now)
    endAt = endAt || process.hrtime();
    // create fake log request
    // these attributes are the required attributes for morgan
    const fakeReq = Object.assign({
        // headers
        headers: req.headers,
        // http method
        method: req.method,
        // url
        url: req.url,
        // remote address
        connection: {
            remoteAddress: req.connection ? req.connection.remoteAddress : req.socket.remoteAddress
        },
        // http version attributes
        httpVersion: req.httpVersion,
        httpVersionMajor: req.httpVersionMajor,
        httpVersionMinor: req.httpVersionMinor,
        // this attribute is used by morgan to calculate response time
        _startAt : startAt,
        // finally add context user to log user name
        context: {
            user: (req.context && req.context.user)
        }
    });
    // create fake log response
    let fakeRes = {
        // set headers sent flag
        headersSent: true,
        // set status code
        statusCode: res.statusCode,
        // fake method to avoid morgan errors
        getHeader: () => {
        },
        // this attribute is used by morgan to calculate response time
        _startAt : endAt
    };
    // get formatLine function of morgan
    const formatLine = morgan.compile(ACCESS_LOG_FORMAT);
    // noinspection JSCheckFunctionSignatures
    const line = formatLine(morgan, fakeReq, fakeRes);
    // write access log data to stream
    accessLogStream.write(line + '\n');
}
