# @universis/access-log

An express middleware for creating apache-like access logs

## Install

    npm i @universis/access-log

## Usage

Use `accessLog` middleware:

    import express from 'express';
    import {accessLog} from '@universis/access-log';

    # create application
    app = express()
    # register access log middleware
    app.use('/', accessLog());

or write access log entries by using `writeAccessLog(req: LogRequest, res: LogResponse, startAt: Date, endAt?: Date)`:

    import express from 'express';
    import {writeAccessLog} from '@universis/access-log';

    # create application
    app = express()
    # register access log middleware
    app.use('/', (req, res) => {
        writeAccessLog(req, res, new Date())
    });

